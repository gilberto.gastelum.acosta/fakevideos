// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBvCreTGGt_sVJKzJ2zWYDO5DpYxADOGX0",
    authDomain: "fakevideos-289323.firebaseapp.com",
    databaseURL: "https://fakevideos-289323.firebaseio.com",
    projectId: "fakevideos-289323",
    storageBucket: "fakevideos-289323.appspot.com",
    messagingSenderId: "789461579746",
    appId: "1:789461579746:web:cb88f54bb516aa8fe42bbb"
  }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
