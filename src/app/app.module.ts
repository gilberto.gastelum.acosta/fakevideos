import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';
//Componentes
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { FavoritosComponent } from './components/favoritos/favoritos.component';
import { RegistroComponent } from './components/registro/registro.component';
//Primeng
import { ButtonModule } from 'primeng/button';
import { DataViewModule } from 'primeng/dataview';
import { InputTextModule } from 'primeng/inputtext'
import {PasswordModule} from 'primeng/password';
//Firebase
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';
///*//*/*//
import { MDBBootstrapModule } from 'angular-bootstrap-md';
//Recaptcha
import { RecaptchaModule,RecaptchaFormsModule  } from 'ng-recaptcha';
//Guards
import { AuthGuard } from "./auth/auth.guard";
import {SecureInnerPaguesGuard} from './auth/secure-inner-pagues.guard';
//**// */
import { ToastrModule } from 'ngx-toastr';
//**/* */
import { AuthService } from './services/auth.service';



const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'favoritos', component: FavoritosComponent, canActivate:[AuthGuard] },
  { path: 'login', component: LoginComponent, canActivate:[SecureInnerPaguesGuard] },
  { path: 'registro', component: RegistroComponent, canActivate:[SecureInnerPaguesGuard] }

]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    LoginComponent,
    RegistroComponent,
    FavoritosComponent
  ],
  imports: [
    MDBBootstrapModule.forRoot(),
    RecaptchaModule,
    RecaptchaFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    BrowserModule,
    RouterModule.forRoot(routes),
    AppRoutingModule,
    ButtonModule,
    PasswordModule,
    InputTextModule,
    DataViewModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    FormsModule
  ],
  exports: [ButtonModule,
    BrowserAnimationsModule,
    DataViewModule],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
