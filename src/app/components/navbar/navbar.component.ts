import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  sesion: boolean = false;
  public usuario: string;

  constructor(private authService: AuthService) {}

  ngOnInit() {
    if (this.authService.sesion == false) {
      if (this.authService.isLoggedIn == true) {
        this.sesion = true;
        this.usuario = this.authService.emailUser;
      }
    }else{
      this.sesion = this.authService.sesion;
      this.usuario = this.authService.emailUser;
    }
  }

  logout() {
    this.authService.SignOut();
    location.reload();
  }
}
