import { Component, OnInit } from '@angular/core';
import {YoutubeService} from 'src/app/Services/youtube.service'; 
import {AuthService} from '../../services/auth.service';
@Component({
  selector: 'app-favoritos',
  templateUrl: './favoritos.component.html',
  styleUrls: ['./favoritos.component.scss']
})
export class FavoritosComponent implements OnInit {
  videosFavoritos:any[];
  sesion:boolean=false;
  usuario: string;
  constructor(private youtubeService:YoutubeService, private authService:AuthService) { }

  ngOnInit(): void {
    if (this.authService.sesion == false) {
      if (this.authService.isLoggedIn == true) {
        this.sesion = true;
        this.usuario = this.authService.emailUser;
        this.getVideosFavoritos(this.usuario);
      }
    }else{
      this.sesion = this.authService.sesion;
      this.usuario = this.authService.emailUser;
      this.getVideosFavoritos(this.usuario);
    }
  }

  getVideosFavoritos(usuario){
    this.youtubeService.getVideosFavoritos(usuario)
    .subscribe(result=>{
      this.videosFavoritos=result;
    });
  }

  deleteVideoFav(id){
    this.youtubeService.deleteFavoritos(id);
    const longi = this.videosFavoritos.length;
     var index:number;
    for (var x=0; x<longi; x++){
      if (this.videosFavoritos[x]["id"]["videoId"]==id){
        this.videosFavoritos[x]["etag"]="favoritess";
       x=longi;
      }
    }
    this.videosFavoritos.splice(index,1);
  }
  
  verVideo(id){
    window.open("https://www.youtube.com/watch?v="+id);
  }
}
