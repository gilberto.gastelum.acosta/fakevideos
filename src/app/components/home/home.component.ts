import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { YoutubeService } from 'src/app/Services/youtube.service';
import { Busqueda } from 'src/app/models/busqueda';
import { AuthService } from '../../services/auth.service';
import { Video } from '../../models/videos';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  resultados: any[];
  sesion: boolean = false;
  videosFavoritos: any[];
  favorito: boolean = false;

  videoFavorito: Video = {
    titulo: '',
    descripcion: '',
    id: '',
    miniatura: '',
    canal: '',
    favorito: true,
    usuario: ''
  }

  @Input() busqueda: Busqueda = {
    palabra_clave: ''
  }
  constructor(private youtubeService: YoutubeService, private authService: AuthService) { }

  ngOnInit(): void {
    if (this.authService.sesion == false) {
      if (this.authService.isLoggedIn == true) {
        this.sesion = true;
        this.videoFavorito.usuario = this.authService.emailUser;
        this.getVideos(this.videoFavorito.usuario);
      }
    } else {
      this.sesion = this.authService.sesion;
      this.videoFavorito.usuario = this.authService.emailUser;
      this.getVideos(this.videoFavorito.usuario);
    }

    this.youtubeService.buscar("").subscribe((data) => {
      this.resultados = data.items;
      this.comprobarFavorito();
    });
  }

  buscarVideos() {
    this.youtubeService.buscar(this.busqueda.palabra_clave).subscribe((data) => {
      this.resultados = data.items;
      this.comprobarFavorito();
    });
  }

  addFavoritos(titulo, miniatura, canal, descripcion, id) {
    this.videoFavorito.canal = canal;
    this.videoFavorito.id = id;
    this.videoFavorito.descripcion = descripcion;
    this.videoFavorito.titulo = titulo;
    this.videoFavorito.miniatura = miniatura;
    this.videoFavorito.favorito = true;

    this.youtubeService.addFavoritos(this.videoFavorito);

    const longi = this.resultados.length;
    var index: number;
    for (var x = 0; x < longi; x++) {
      if (this.resultados[x]["id"]["videoId"] == id) {
        this.resultados[x]["etag"] = "favorite";
        x = longi;
      }
    }
    this.comprobarFavorito();
  }

  getVideos(usuario) {
    this.youtubeService.getVideosFavoritos(usuario)
      .subscribe(result => {
        this.videosFavoritos = result;
      });
  }

  deleteVideoFav(id) {
    this.youtubeService.deleteFavoritos(id);
    const longi = this.resultados.length;
    for (var x = 0; x < longi; x++) {
      if (this.resultados[x]["id"]["videoId"] == id) {
        this.resultados[x]["etag"] = "favoritess";
        x = longi;
      }
    }
  }

  comprobarFavorito() {
    const longResultados = this.resultados.length;
    const longVideosFav = this.videosFavoritos.length;
    for (var x = 0; x < longVideosFav; x++) {
      this.favorito = false;
      for (var y = 0; y < longResultados; y++) {
        if (this.videosFavoritos[x]["id"] == this.resultados[y]["id"]["videoId"]) {
          this.resultados[y]["etag"] = "favorite";
        }
      }
    }
  }

  verVideo(id){
    window.open("https://www.youtube.com/watch?v="+id);
  }

}
