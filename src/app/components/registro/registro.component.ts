import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from '../../services/user.service';
import { User } from '../../models/usuarios';
import { ToastrService } from 'ngx-toastr';

export interface FormModel {
  captcha?: string;
}
@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.scss']
})

export class RegistroComponent implements OnInit {
  public formModel: FormModel = {};
  recaptcha: any[];
  confirmPwd:string;
  pwd:string;

  newUser: User = {
    nombres: '',
    apellidos: '',
    email: '',
    id: ''
  };

  key: String;

  constructor(private authService: AuthService, private userService: UserService, private toastr:ToastrService) { }

  ngOnInit(): void {
    this.key = "6Lcf1csZAAAAAOK-PdXOiwvK_XI6CVC9BzgvEgyo";
  }

  registrar(email, pwd,newUser:User) {
    this.userService.SignUp(email, pwd,newUser)
    .then((result)=>{
    }).catch((error)=>{
      this.toastr.error("Este email se encuentra en uso");
    })
  }

  registrarUsuario(user: User) {
    this.userService.registrarUsuario(this.newUser);
  }

}
