import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  email:string;
  pwd:string;

  constructor(private authService:AuthService) { }

  login(email,pwd){
    this.authService.SignIn(email,pwd);
  }

  ngOnInit(): void {
  }

}
