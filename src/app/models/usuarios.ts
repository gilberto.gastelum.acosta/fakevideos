export interface User {
    nombres: string,
    apellidos: string,
    email: string,
    id:string
}