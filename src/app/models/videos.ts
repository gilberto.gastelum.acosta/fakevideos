export class Video{
    id:string;
    miniatura:string;
    canal:string;
    descripcion:string;
    titulo:string;
    usuario:string;
    favorito:boolean;
}