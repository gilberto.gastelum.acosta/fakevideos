import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Video} from '../models/videos';
import { AngularFirestore } from '@angular/fire/firestore';
import { ToastrService } from 'ngx-toastr';
@Injectable({
  providedIn: 'root'
})
export class YoutubeService {
  resultados = 50;
  API_KEY = "AIzaSyC9NYXzy7Dyhhd4GypUMO0mQqAVjl89BEM";
  constructor(private http: HttpClient, public afs: AngularFirestore, private toastr:ToastrService) { }

  buscar(palabra): Observable<any> {
    const url = "https://www.googleapis.com/youtube/v3/search?key=" + this.API_KEY + "&part=snippet&q=" + palabra +"&type=video" +"&maxResults=" + this.resultados;
    return this.http.get<any>(url);
  }

  addFavoritos(video:Video){
    return this.afs.collection('videos').doc(video.id).set(video)
    .then((result)=>{
      this.toastr.success("Video añadido a favoritos")
    }).catch((error)=>{
      this.toastr.error(error.message);
    })
  }

  deleteFavoritos(id){
    return this.afs.collection('videos').doc(id).delete()
    .then((result)=>{
     this.toastr.warning("Video eliminado de favoritos");
    }).catch((error)=>{
      this.toastr.error(error.message);
    });
  }

  getVideosFavoritos(usuario){
    return this.afs.collection('videos',ref => ref.where ('usuario','==',usuario)).valueChanges();
  }
}
