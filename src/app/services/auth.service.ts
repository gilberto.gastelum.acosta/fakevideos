import { Injectable, NgZone } from '@angular/core';
import '@firebase/auth';
import { User } from '../models/usuarios';
import { UserService } from '../services/user.service';
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Router } from "@angular/router";
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public datosUser :any[];
  public sesion:boolean=false;
  public emailUser: string;
  public userData: any; 
  constructor(
    public afs: AngularFirestore,   
    public afAuth: AngularFireAuth, 
    public router: Router,
    public ngZone: NgZone, 
    private userService: UserService,
    private toastr:ToastrService

  ) {

    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.userData = user;
        localStorage.setItem('user', JSON.stringify(this.userData));
        JSON.parse(localStorage.getItem('user'));
      } else {
        localStorage.setItem('user', null);
        JSON.parse(localStorage.getItem('user'));
      }
    });
  }

  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    if (user!==null){
      this.emailUser=(user["email"]);
    }
    return (user !== null) ? true : false;
  }

  SetUserData(user) {
    const id= user[0]["data"]["id"];
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`usuarios/${id}`);
    const userData: User = {
      nombres: user[0]["data"]["nombres"],
      apellidos: user[0]["data"]["apellidos"],
      email: user[0]["data"]["email"],
      id: user[0]["data"]["id"]
    }
    return userRef.set(userData, {
      merge: true
    });
  }

  SignIn(email, password) {
    return this.afAuth.signInWithEmailAndPassword(email, password)
      .then((result) => {
        this.ngZone.run(() => {
          this.emailUser=email
          this.sesion=true;
          this.router.navigate(['/']);
        });
        this.getUserInfo(email);
      }).catch((error) => {
      this.toastr.error("Email o contraseña incorrectos")
      });
  }

  getUserInfo(email) {
    this.userService.getInfoUser(email).subscribe
      (result => {
        this.datosUser = result;
        this.SetUserData(this.datosUser);
      });
  }

  registrarUsuario(user: User) {
    return this.afs.collection('usuarios').add(user)
      .then((result) => {
        this.toastr.success("Registro correcto");
      }).catch((error) => {
        this.toastr.error(error);
      });
  }

  SignOut() {
    return this.afAuth.signOut().then(() => {
      localStorage.removeItem('user');
    });
  }
}
