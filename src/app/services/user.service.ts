import { Injectable, NgZone } from '@angular/core';
import '@firebase/auth';
import { User } from '../models/usuarios';
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from '../services/auth.service';
import { Router } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public sesion: boolean = false;
  public emailUser: string;
  constructor(public afs: AngularFirestore, public afAuth: AngularFireAuth, private toastr: ToastrService, public router: Router,
    public ngZone: NgZone,) { }

  SignUp(email, password, newUser: User) {
    return this.afAuth.createUserWithEmailAndPassword(email, password)
      .then((result) => {
        this.registrarUsuario(newUser);
        this.afAuth.signInWithEmailAndPassword(email, password)
          .then((result) => {
            this.ngZone.run(() => {
              this.emailUser = email
              this.sesion = true;
              this.router.navigate(['/']);
            });
          });
      }).catch((error) => {
        this.toastr.error("Este email se encuentra en uso");
      });
  }

  registrarUsuario(user: User) {
    user.id = this.afs.createId();
    return this.afs.collection('usuarios').doc(user.id).set(user)
      .then((result) => {
      }).catch((error) => {
        this.toastr.error(error);
      });
  }

  getInfoUser(email) {
    return this.afs.collection('usuarios', ref => ref.where('email', '==', email)).snapshotChanges().pipe
      (map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          return { data };
        });
      }));
  }
}
